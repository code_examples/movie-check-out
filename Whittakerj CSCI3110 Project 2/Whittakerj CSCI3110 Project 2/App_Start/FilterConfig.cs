﻿using System.Web;
using System.Web.Mvc;

namespace Whittakerj_CSCI3110_Project_2
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
