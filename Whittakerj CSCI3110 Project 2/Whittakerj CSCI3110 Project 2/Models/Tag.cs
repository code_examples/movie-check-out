﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Whittakerj_CSCI3110_Project_2.Models
{
    public class Tag
    {
        [Key]
        public int Id { get; set; }
        public String TagText { get; set; }
    }
}
