﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Whittakerj_CSCI3110_Project_2.Models
{
    public class _WatchedMovieInfo
    {
        public IEnumerable<Movie> Movies { get; set; }
        public IEnumerable<WatchedMovie> WatchedMovies { get; set; }
    }
}
