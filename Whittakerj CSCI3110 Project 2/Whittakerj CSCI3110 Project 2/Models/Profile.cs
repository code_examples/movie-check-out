﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Whittakerj_CSCI3110_Project_2.Models
{
    public class Profile
    {
        [Key]
        public string Email { get; set; }
        public string Username { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "First name must be less than 50 characters.")]
        public string FirstName { get; set; }
        [StringLength(50, ErrorMessage = "Last name must be less than 50 characters.")]
        public string LastName { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        [StringLength(2, ErrorMessage = "Must be 2 characters and a valid US postal code")]
        public string State { get; set; }
        [Required]
        [StringLength(5, ErrorMessage = "Must be 5 characters")]
        public string Zip { get; set; }
    }
}
