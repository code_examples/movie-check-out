﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Whittakerj_CSCI3110_Project_2.Models
{
    public class Movie
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Title must be less than 50 characters.")]
        public string Title { get; set; }
        [RegularExpression(@"^19[0-9]\d|20[0-9]\d|2099$", ErrorMessage = "Invalid year")]
        public string Year { get; set; }
        public int LengthInMinutes { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public double Price { get; set; }
        [Required]
        public string IMDBUrl { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
    }


}
