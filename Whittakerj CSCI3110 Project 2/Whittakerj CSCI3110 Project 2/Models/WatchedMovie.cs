﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Whittakerj_CSCI3110_Project_2.Models
{
    public class WatchedMovie
    {
        [Key]
        [Column(Order = 0)]
        public string email { get; set; }
        [Key]
        [Column(Order = 1)]
        public int MovieId { get; set; }
        public int TimesViewed { get; set; }
    }
}
