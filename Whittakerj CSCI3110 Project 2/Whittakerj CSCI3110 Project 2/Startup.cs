﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Whittakerj_CSCI3110_Project_2.Startup))]
namespace Whittakerj_CSCI3110_Project_2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
