﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Whittakerj_CSCI3110_Project_2.Models;

namespace Whittakerj_CSCI3110_Project_2.DataContexts
{
    public class MovieDb : DbContext
    {
        public MovieDb() : base("name=DefaultConnection")
        {
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<WatchedMovie> WatchedMovies { get; set; }
    }

}
