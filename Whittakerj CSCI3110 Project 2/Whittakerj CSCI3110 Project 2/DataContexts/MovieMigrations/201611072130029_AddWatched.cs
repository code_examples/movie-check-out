namespace Whittakerj_CSCI3110_Project_2.DataContexts.MovieMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddWatched : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WatchedMovies",
                c => new
                    {
                        email = c.String(nullable: false, maxLength: 128),
                        MovieId = c.Int(nullable: false),
                        TimesViewed = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.email, t.MovieId });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.WatchedMovies");
        }
    }
}
