﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Whittakerj_CSCI3110_Project_2.DataContexts;
using Whittakerj_CSCI3110_Project_2.Models;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace Whittakerj_CSCI3110_Project_2.Controllers
{

    public class ProfileController : Controller
    {

        private IdentityDb _idb = new IdentityDb();

        private MovieDb _db = new MovieDb();

        protected override void Dispose(bool disposing)
        {
            _idb.Dispose();
            _db.Dispose();
            base.Dispose(disposing);
        }

        // GET: profile
        public ActionResult Create()
        {
            //Since we know the user will have an Asp.net Identity, we do not generally need to pass ids and will just get their Username
            //from their identity. This is used throughout this entire controller.
            string userName = User.Identity.GetUserName();
            Profile profile = _db.Profiles.Find(userName);
            if (profile == null)
            {
                ViewBag.userName = userName;
                return View();
            }
            return RedirectToAction("Details");
        }

        [HttpPost]
        public ActionResult Create(Profile profile)
        {
            if (ModelState.IsValid)
            {
                _db.Profiles.Add(profile);
                _db.SaveChanges();
                return RedirectToAction("Details");
            }

            return RedirectToAction("Details");
        }

        public ActionResult Details()
        {
            string userName = User.Identity.GetUserName();
            Profile profile = _db.Profiles.Find(userName);
            if (profile == null)
            {
                return RedirectToAction("Create");
            }
            return View(profile);
        }

        public ActionResult Edit()
        {
            string userName = User.Identity.GetUserName();
            Profile profile = _db.Profiles.Find(userName);
            if (profile == null)
            {
                return HttpNotFound();
            }
            ViewBag.userName = userName;
            return View(profile);
        }

        [HttpPost]
        public ActionResult Edit(Profile profile)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(profile).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Details");
            }
            return View(profile);
        }

        public ActionResult Delete()
        {
            string userName = User.Identity.GetUserName();
            Profile profile = _db.Profiles.Find(userName);
            if (profile == null)
            {
                return HttpNotFound();
            }
            return View(profile);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult ConfirmDelete()
        {
            string userName = User.Identity.GetUserName();
            Profile profile = _db.Profiles.Find(userName);
            _db.Profiles.Remove(profile);
            _db.SaveChanges();
            return RedirectToAction("Create");
        }
    }
}