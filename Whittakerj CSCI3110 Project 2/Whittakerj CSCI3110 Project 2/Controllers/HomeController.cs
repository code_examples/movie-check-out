﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Whittakerj_CSCI3110_Project_2.DataContexts;
using Whittakerj_CSCI3110_Project_2.Models;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace Whittakerj_CSCI3110_Project_2.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        private IdentityDb _idb = new IdentityDb();

        private MovieDb _db = new MovieDb();

        protected override void Dispose(bool disposing)
        {
            _idb.Dispose();
            _db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Index(Tag tag)
        {
            string userName = User.Identity.GetUserName();
            Profile profile = _db.Profiles.Find(userName);
            //Admin can't see this
            if (User.IsInRole("Admin"))
            {
                return RedirectToAction("Index", "Admin");
            }
            //User must create a profile
            if (profile == null)
            {
                return RedirectToAction("Create", "Profile");
            }

            //Passing all movies into the viewbag so we can compare them on the index page.
            ViewBag.Movies = _db.Movies.ToList();
            ViewBag.Username = userName;
            return View(_db.WatchedMovies.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
    }
}