﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Whittakerj_CSCI3110_Project_2.DataContexts;
using Whittakerj_CSCI3110_Project_2.Models;

namespace Whittakerj_CSCI3110_Project_2.Controllers
{
    //Admins only
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {

        private IdentityDb _idb = new IdentityDb();

        private MovieDb _db = new MovieDb();

        protected override void Dispose(bool disposing)
        {
            _idb.Dispose();
            _db.Dispose();
            base.Dispose(disposing);
        }


        //Standard CRUD controls. Nothing special here.
        public ActionResult Index()
        {
            return View(_db.Movies.ToList());
        }

        public ActionResult CreateMovie()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateMovie(Movie movie)
        {
            if (ModelState.IsValid)
            {
                _db.Movies.Add(movie);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }


        public ActionResult MovieDetails(int id)
        {
            Movie movie = _db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        public ActionResult EditMovie(int id)
        {
            Movie movie = _db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        [HttpPost]
        public ActionResult EditMovie(Movie movie)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(movie).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(movie);
        }


        public ActionResult DeleteMovie(int id)
        {
            Movie movie = _db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        [HttpPost, ActionName("DeleteMovie")]
        public ActionResult ConfirmDelete(int id)
        {
            Movie movie = _db.Movies.Find(id);
            _db.Movies.Remove(movie);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}