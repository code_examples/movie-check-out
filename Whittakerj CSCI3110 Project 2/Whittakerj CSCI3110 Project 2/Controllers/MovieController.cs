﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Whittakerj_CSCI3110_Project_2.DataContexts;
using Whittakerj_CSCI3110_Project_2.Models;
using System.Data.Entity;
using Microsoft.AspNet.Identity;

namespace Whittakerj_CSCI3110_Project_2.Controllers
{
    public class MovieController : Controller
    {

        private IdentityDb _idb = new IdentityDb();

        private MovieDb _db = new MovieDb();

        protected override void Dispose(bool disposing)
        {
            _idb.Dispose();
            _db.Dispose();
            base.Dispose(disposing);
        }

        // GET: Movie
        public ActionResult Index()
        {
            string userName = User.Identity.GetUserName();
            Profile profile = _db.Profiles.Find(userName);
            //If they are an admin, they can't be here.

            if (User.IsInRole("Admin"))
            {
                return RedirectToAction("Index", "Admin");
            }

            //User must create a profile
            if (profile == null)
            {
                return RedirectToAction("Create", "Profile");
            }

            return View(_db.Movies.ToList());
        }

        public ActionResult Details(int id)
        {
            Movie movie = _db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        //This will show the pay screen.
        public ActionResult Watch(int id)
        {
            Movie movie = _db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        [HttpPost]
        public ActionResult Watch(Movie movie)
        {
            if (ModelState.IsValid)
            {
                string userName = User.Identity.GetUserName();
                var id = movie.Id;
                //Search for the watched movie using the Username and the id of the movie
                WatchedMovie watchedmovie = _db.WatchedMovies.Find(userName, id);
                //If it doesn't exist, create it. It means it's the user's first time watching it.
                if (watchedmovie == null)
                {
                    watchedmovie = new WatchedMovie();
                    watchedmovie.email = userName;
                    watchedmovie.MovieId = id;
                    watchedmovie.TimesViewed = 1;
                    _db.WatchedMovies.Add(watchedmovie);
                    _db.SaveChanges();
                    return RedirectToAction("Index", "Home");
                }
                //if it does exist, just increment TimesViewed by 1.
                watchedmovie.TimesViewed = watchedmovie.TimesViewed + 1;
                _db.Entry(watchedmovie).State = EntityState.Modified;
                _db.SaveChanges();

                return Redirect(movie.IMDBUrl);
            }
            return View(movie);
        }

        //Rewatch movie. No real validation here, but it wasn't asked for.
        //The code is mostly the same as the previous watch, it just doesn't ask for the user to pay so it doesn't need an Httppost.
        public ActionResult WatchAgain(int id)
        {
            string userName = User.Identity.GetUserName();
            WatchedMovie watchedmovie = _db.WatchedMovies.Find(userName, id);
            Movie movie = _db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            watchedmovie.TimesViewed = watchedmovie.TimesViewed + 1;
            _db.Entry(watchedmovie).State = EntityState.Modified;
            _db.SaveChanges();
            return Redirect(movie.IMDBUrl);
        }

        public ActionResult Tags(int id)
        {
            Movie movie = _db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        public ActionResult TagsCreate(int id)
        {
            return View();
        }

        public ActionResult TagsEdit(int id)
        {
            return View();
        }

        public ActionResult TagsDelete(int id)
        {
            return View();
        }
    }
}
